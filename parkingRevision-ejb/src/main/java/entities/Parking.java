package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Parking implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String designation;
	private String adresse;
	private int capacite;
	@OneToMany(mappedBy="parking")
	private List<Zone>zones;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public int getCapacite() {
		return capacite;
	}
	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}
	public List<Zone> getZones() {
		return zones;
	}
	public void setZones(List<Zone> zones) {
		this.zones = zones;
	}
	public Parking(int id, String designation, String adresse, int capacite, List<Zone> zones) {
		super();
		this.id = id;
		this.designation = designation;
		this.adresse = adresse;
		this.capacite = capacite;
		this.zones = zones;
	}
	public Parking() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Parking(String designation, String adresse, int capacite) {
		super();
		this.designation = designation;
		this.adresse = adresse;
		this.capacite = capacite;
	}
	@Override
	public String toString() {
		return "Parking [id=" + id + ", designation=" + designation + ", adresse=" + adresse + ", capacite=" + capacite
				+ "]";
	}
	
}
