package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Personnel implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String prenom;
	private int age;
	private Date dateDeRecrutement;
	private String login;
	private String password;
	@Enumerated(EnumType.STRING)
	private Poste poste;
	@OneToOne
	private Zone zonePersonnel;
	@ManyToOne
	private Zone zoneGarde;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getDateDeRecrutement() {
		return dateDeRecrutement;
	}
	public void setDateDeRecrutement(Date dateDeRecrutement) {
		this.dateDeRecrutement = dateDeRecrutement;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Poste getPoste() {
		return poste;
	}
	public void setPoste(Poste poste) {
		this.poste = poste;
	}
	public Zone getZonePersonnel() {
		return zonePersonnel;
	}
	public void setZonePersonnel(Zone zonePersonnel) {
		this.zonePersonnel = zonePersonnel;
	}
	public Zone getZoneGarde() {
		return zoneGarde;
	}
	public void setZoneGarde(Zone zoneGarde) {
		this.zoneGarde = zoneGarde;
	}
	public Personnel(int id, String nom, String prenom, int age, Date dateDeRecrutement, String login, String password,
			Poste poste, Zone zonePersonnel, Zone zoneGarde) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.dateDeRecrutement = dateDeRecrutement;
		this.login = login;
		this.password = password;
		this.poste = poste;
		this.zonePersonnel = zonePersonnel;
		this.zoneGarde = zoneGarde;
	}
	public Personnel() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Personnel [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", age=" + age + ", dateDeRecrutement="
				+ dateDeRecrutement + ", login=" + login + ", password=" + password + ", poste=" + poste + "]";
	}
	public Personnel(String nom, String prenom, int age, Date dateDeRecrutement, String login, String password,
			Poste poste) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.dateDeRecrutement = dateDeRecrutement;
		this.login = login;
		this.password = password;
		this.poste = poste;
	}
	
	
	
}
