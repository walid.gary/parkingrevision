package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Zone implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String ref;
	private float dimension;
	@OneToMany(mappedBy="zoneGarde")
	private List<Personnel> personnels;
	@OneToOne(mappedBy="zonePersonnel")
	private Personnel personnel;
	@ManyToOne
	private Parking parking;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public float getDimension() {
		return dimension;
	}
	public void setDimension(float dimension) {
		this.dimension = dimension;
	}
	public List<Personnel> getPersonnels() {
		return personnels;
	}
	public void setPersonnels(List<Personnel> personnels) {
		this.personnels = personnels;
	}
	public Personnel getPersonnel() {
		return personnel;
	}
	public void setPersonnel(Personnel personnel) {
		this.personnel = personnel;
	}
	public Parking getParking() {
		return parking;
	}
	public void setParking(Parking parking) {
		this.parking = parking;
	}
	public Zone(int id, String ref, float dimension, List<Personnel> personnels, Personnel personnel, Parking parking) {
		super();
		this.id = id;
		this.ref = ref;
		this.dimension = dimension;
		this.personnels = personnels;
		this.personnel = personnel;
		this.parking = parking;
	}
	public Zone() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Zone [id=" + id + ", ref=" + ref + ", dimension=" + dimension + "]";
	}
	public Zone(String ref, float dimension) {
		super();
		this.ref = ref;
		this.dimension = dimension;
	}
	
}
