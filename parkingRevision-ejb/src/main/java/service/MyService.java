package service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entities.Parking;
import entities.Personnel;
import entities.Poste;
import entities.Zone;

/**
 * Session Bean implementation class MyService
 */
@Stateless
@LocalBean
public class MyService implements MyServiceRemote {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	EntityManager em;
    public MyService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void ajouterPersonnel(Personnel personnel) {
		em.persist(personnel);
		
	}

	@Override
	public void ajoutParkingetZones(Parking parking, List<Zone> listeZones) {
		em.persist(parking);
		for(Zone z : listeZones) {
			z.setParking(parking);
			em.persist(z);
		}
		
	}

	@Override
	public List<Personnel> listerPersonnel() {
		TypedQuery<Personnel> query = em.createQuery("SELECT f FROM Personnel f",Personnel.class);  
	 	   try {
	 		 return query.getResultList();
	 	   }
	 	   catch (Exception e){
	 		   System.out.println("erreur");	   
	 	   }
	 	   return null;
	}

	@Override
	public void affecterPersonnelZone(int idzone, int idGarde, Poste poste) {
		Zone z = em.find(Zone.class, idzone);
		Personnel p = em.find(Personnel.class, idGarde);
		if (poste.equals(Poste.RESPONSABLE)) {
			p.setZonePersonnel(z);
		}
		else {
			p.setZoneGarde(z);
		}
		em.merge(p);
		
	}
	@Override
	public Personnel userlogin(String login, String passwoord) {
		TypedQuery<Personnel> qr= em.createQuery("select e from Personnel e where e.login=:param1 and e.password=:param2", Personnel.class);
		qr.setParameter("param1", login );
		qr.setParameter("param2", passwoord);
		Personnel u = null;
		try {
			
			 u= qr.getSingleResult();
			
		} catch (NoResultException e ) {
			Logger.getAnonymousLogger();
		}
		
			return u;
	}

	@Override
	public Zone GetZoneByGarde(int id) {
		System.out.println(em.find(Zone.class, id)+"Service");

		return em.find(Zone.class, id);
	}
	@Override
	public Zone GetZoneByResp(int id) {
		System.out.println(em.find(Zone.class, id)+"Service");

		return em.find(Zone.class, id);
	}

	@Override
	public List<Personnel> listerGarde() {
		TypedQuery<Personnel> q=em.createQuery("select e from Personnel e where e.poste NOT LIKE 'responsable'", Personnel.class);
		
		return q.getResultList();
	}

	@Override
	public Personnel getP(String nom) {
		TypedQuery<Personnel> qr= em.createQuery("select e from Personnel e where e.nom=:nom", Personnel.class);
		qr.setParameter("nom", nom );
		
		Personnel u = null;
		try {
			
			 u= qr.getSingleResult();
			
			
		} catch (NoResultException e ) {
			Logger.getAnonymousLogger();
		}
		
			return u;

	}

}
