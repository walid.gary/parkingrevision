package service;

import java.util.List;

import javax.ejb.Remote;

import entities.Parking;
import entities.Personnel;
import entities.Poste;
import entities.Zone;

@Remote
public interface MyServiceRemote {
	public void ajouterPersonnel(Personnel personnel);
	public void ajoutParkingetZones(Parking parking, List<Zone>listeZones);
	public List<Personnel> listerPersonnel();
	public void affecterPersonnelZone(int idzone, int idGarde, Poste poste); 
	public Personnel userlogin(String login, String passwoord);
	public Personnel getP(String nom);
	public Zone GetZoneByGarde (int id);
	Zone GetZoneByResp(int id);
	public List<Personnel> listerGarde();

}
