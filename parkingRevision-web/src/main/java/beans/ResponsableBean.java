package beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import entities.Personnel;
import entities.Zone;
import service.MyService;

@ManagedBean
@SessionScoped
public class ResponsableBean {

	@EJB
	MyService cm;
	
	@ManagedProperty(value="#{loginBean}")
	private LoginBean lb;
	private List<Personnel> ls = new ArrayList<Personnel>();
	private List<String> liste = new ArrayList<String>();
	private String aaa =null;
	private String bbb =null;
	public List<String> getListe() {
		return liste;
	}
	public void setListe(List<String> liste) {
		this.liste = liste;
	}
	public String getAaa() {
		return aaa;
	}
	public void setAaa(String aaa) {
		this.aaa = aaa;
	}
	private Zone z ;
	@PostConstruct
	public void init() {
		z=cm.GetZoneByResp(lb.getE().getZonePersonnel().getId());
		ls=cm.listerGarde();
		for (Personnel p : ls) {
			 aaa = p.getNom();
			 bbb = aaa+" "+p.getPrenom()+" - Poste : "+p.getPoste();
			 liste.add(bbb);
		}
	
	}
	public String getBbb() {
		return bbb;
	}
	public void setBbb(String bbb) {
		this.bbb = bbb;
	}
	public void affecterGarde () {
		System.out.println(aaa);
		Personnel p = new Personnel();
		p=cm.getP(aaa);
		if (p.getZoneGarde()==null) {
			cm.affecterPersonnelZone(z.getId(), p.getId(), p.getPoste());
		}
		else {
			FacesContext.getCurrentInstance().addMessage("form:btn",
					new FacesMessage(FacesMessage.SEVERITY_WARN, "affectation failed",
					"Garde indisponible"));
		}
	}

	public List<Personnel> getLs() {
		return ls;
	}
	public void setLs(List<Personnel> ls) {
		this.ls = ls;
	}
	public MyService getCm() {
		return cm;
	}
	public void setCm(MyService cm) {
		this.cm = cm;
	}
	public LoginBean getLb() {
		return lb;
	}
	public void setLb(LoginBean lb) {
		this.lb = lb;
	}
	public Zone getZ() {
		return z;
	}
	public void setZ(Zone z) {
		this.z = z;
	}
}
