package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import entities.Zone;
import service.MyService;

@ManagedBean
@SessionScoped
public class GardeBean implements Serializable{
	
	@EJB
	MyService cm;
	
	@ManagedProperty(value="#{loginBean}")
	private LoginBean lb;
	
	private String nom;
	private Zone z ;
	private List<Zone> ls = new ArrayList<Zone>();
	
	public List<Zone> getLs() {
		return ls;
	}
	public void setLs(List<Zone> ls) {
		this.ls = ls;
	}
	@PostConstruct
	public void init () {
		System.out.println("sdqhdsqklmdq");
		z = cm.GetZoneByGarde(lb.getE().getZoneGarde().getId());ls.add(z);
		System.out.println(z);
		nom = lb.getE().getNom()+" "+lb.getE().getPrenom();
		System.out.println(nom);
		
	}
	public MyService getCm() {
		return cm;
	}

	public void setCm(MyService cm) {
		this.cm = cm;
	}

	public LoginBean getLb() {
		return lb;
	}

	public void setLb(LoginBean lb) {
		this.lb = lb;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}



	public Zone getZ() {
		return z;
	}

	public void setZ(Zone z) {
		this.z = z;
	}

	
}
