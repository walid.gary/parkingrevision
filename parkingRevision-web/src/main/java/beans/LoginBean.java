package beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import entities.Personnel;
import entities.Poste;
import service.MyService;

@ManagedBean
@SessionScoped
public class LoginBean implements Serializable{
	private String login;
	private String password;
	private Personnel e = new Personnel();
	
	@EJB
	MyService cm;
	
	@PostConstruct
	public void init() {
		
	}
	
	
	public String authentification()
	{
		String navigTo="null";
		
		  e= cm.userlogin(login, password);
		if((e !=null) && (e.getPoste()==Poste.GARDE_JOUR ||e.getPoste()==Poste.GARDE_NUIT))
		{
			navigTo="/pages/Garde?faces-redirect=true";
			
		}
		else if ((e !=null) && (e.getPoste()==Poste.RESPONSABLE)) 
		{
			navigTo="/pages/Responsable?faces-redirect=true";
			
		}
		else{

				FacesContext.getCurrentInstance().addMessage("form:btn",
							new FacesMessage(FacesMessage.SEVERITY_WARN, "Login failed",
										"Invalid or unknown credentials."));

					return null;
		}
		return navigTo;

	}
	public String doLogout(){
		return "/Login?faces-redirect=true";
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Personnel getE() {
		return e;
	}
	public void setE(Personnel e) {
		this.e = e;
	}
	public MyService getCm() {
		return cm;
	}
	public void setCm(MyService cm) {
		this.cm = cm;
	}

	
}
